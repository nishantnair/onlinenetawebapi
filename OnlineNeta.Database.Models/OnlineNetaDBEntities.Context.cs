﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnlineNeta.Database.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class OnlineNeta_DB_Entities : DbContext
    {
        public OnlineNeta_DB_Entities()
            : base("name=OnlineNeta_DB_Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<AreaCategory> AreaCategories { get; set; }
        public virtual DbSet<BloodGroup> BloodGroups { get; set; }
        public virtual DbSet<Caste> Castes { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Configuration> Configurations { get; set; }
        public virtual DbSet<ConfigurationType> ConfigurationTypes { get; set; }
        public virtual DbSet<Constituency> Constituencies { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Education> Educations { get; set; }
        public virtual DbSet<Election> Elections { get; set; }
        public virtual DbSet<ElectionBooth> ElectionBooths { get; set; }
        public virtual DbSet<ElectionCandidate> ElectionCandidates { get; set; }
        public virtual DbSet<ElectionCandidateRepresentative> ElectionCandidateRepresentatives { get; set; }
        public virtual DbSet<ElectionType> ElectionTypes { get; set; }
        public virtual DbSet<FilterCriteria> FilterCriterias { get; set; }
        public virtual DbSet<Gender> Genders { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<ListPart> ListParts { get; set; }
        public virtual DbSet<Occupation> Occupations { get; set; }
        public virtual DbSet<Party> Parties { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<QuestionOption> QuestionOptions { get; set; }
        public virtual DbSet<Religion> Religions { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Survey> Surveys { get; set; }
        public virtual DbSet<SurveyQuestion> SurveyQuestions { get; set; }
        public virtual DbSet<SurveyQuestionType> SurveyQuestionTypes { get; set; }
        public virtual DbSet<SurveyQuetsionAnswer> SurveyQuetsionAnswers { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserConfiguration> UserConfigurations { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
    }
}
