//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnlineNeta.Database.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Question
    {
        public string Description { get; set; }
        public int SurveyQuestionTypeId { get; set; }
        public string Code { get; set; }
        public int Id { get; set; }
    
        public virtual SurveyQuestionType SurveyQuestionType { get; set; }
    }
}
